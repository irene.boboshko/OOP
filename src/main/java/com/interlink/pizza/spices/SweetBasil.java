package com.interlink.pizza.spices;

import com.interlink.pizza.Spices;

public class SweetBasil extends Spices {

    private double price = 7.25;

    public String toString() {
        return String.format("%-12s%15.2f", "Sweet basil", this.price) + " UH";
    }


    public double calculate(int portion) {
        return portion* price;
    }
}
