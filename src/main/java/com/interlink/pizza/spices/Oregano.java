package com.interlink.pizza.spices;

import com.interlink.pizza.Spices;

public class Oregano extends Spices {

    private double price = 13.50;

    public String toString() {
        return String.format("%-12s%15.2f", "Oregano", this.price) + " UH";
    }


    public double calculate(int portion) {
        return portion* price;
    }
}
