package com.interlink.pizza.spices;

import com.interlink.pizza.Spices;

public class Garlic extends Spices {

    private double price = 13.90;

    public String toString() {
        return String.format("%-12s%15.2f", "Garlic", this.price) + " UH";
    }


    public double calculate(int portion) {
        return portion * price;
    }
}
