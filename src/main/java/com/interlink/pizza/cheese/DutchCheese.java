package com.interlink.pizza.cheese;

import com.interlink.pizza.Cheese;

public class DutchCheese extends Cheese {

    public String getName() {
        return "Dutch Cheese";
    }

    private double price = 15.25;

    public String toString() {
        return String.format("%-12s%15.2f", "Dutch cheese", this.price) + " UH";
    }

    public double calculate(int portion) {
        return (portion * price);
    }
}
