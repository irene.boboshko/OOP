package com.interlink.pizza.cheese;

import com.interlink.pizza.Cheese;

public class Ricotta extends Cheese {

    public String getName() {
        return "Ricotta";
    }

    private double price = 28.75;

    public String toString() {
        return String.format("%-12s%15.2f", "Ricotta", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
