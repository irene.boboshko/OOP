package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Pepper extends Vegetable {

    public String toString() {
        double price = 6.50;
        return String.format("%-12s%15.2f", "Pepper", price) + " UH";
    }
}
