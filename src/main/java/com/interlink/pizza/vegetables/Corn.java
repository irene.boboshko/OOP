package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Corn extends Vegetable {

    public String toString() {
        double price = 8.00;
        return String.format("%-12s%15.2f", "Corn", price) + " UH";
    }
}
