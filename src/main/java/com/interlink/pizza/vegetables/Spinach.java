package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Spinach extends Vegetable {

    public String toString() {
        double price = 6.80;
        return String.format("%-12s%15.2f", "Spinach", price) + " UH";
    }
}
