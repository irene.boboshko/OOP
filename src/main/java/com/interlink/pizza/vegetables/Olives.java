package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Olives extends Vegetable {

    private double price = 12.20;

    public String toString() {
        return String.format("%-12s%15.2f", "Olives", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion * price;
    }
}
