package com.interlink.pizza.vegetables;

import com.interlink.pizza.Vegetable;

public class Peas extends Vegetable {

    public String toString() {
        double price = 7.00;
        return String.format("%-12s%15.2f", "Peas", price) + " UH";
    }
}
