package com.interlink.pizza;

class Options {
    void selectOwnPizza() {
        System.out.format("%s\n", "Select one product (cheese) from the menu list and insert its full name in the format: \"dutchcheese\".\nAfter that enter the number of portions you'd like to order.\n\tNote that dough and sauce are the obligatory ingredients for the basic self-made pizza.\nIf you want to quit the program, press \"quit\".\n\n");
    }

    void selectPizza() {
        System.out.format("%s\n", "Select one name from the following list and after that insert the number of portions you'd like to order:\n\t Enter \"nap\" to choose Napoletana.\n\t Enter \"mar\" to choose Marinara.\n\t Enter \"mag\" to choose Margherita.\n\t Enter \"cap\" to choose Caprizzioza.\n Press \"quit\" to quit the program.\n");
    }

    void getPortionsOption() {
        System.out.println("Enter the number of portions to order (digit from 1 to 100)\n");
    }

    void addProductsOption() {
        System.out.println(" Press \"prod\" to add the products.\n\n");
    }

    void getCalculateOption() {
        System.out.println(" Press \"calc\" to get a bill or choose one more option from the variants listed above.\n\n");
    }

    void quit() {
        System.out.println("Thank you for being with us. Hope to see you next time!");
    }

    void showIncorrectInput() {
        System.out.println("The input data is incorrect. Please, try again");
    }
}
