package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Ham extends Fillings {

    private double price = 23.45;

    public String toString() {
        return String.format("%-12s%15.2f", "Ham", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
