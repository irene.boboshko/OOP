package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Egg extends Fillings {

    private double price = 5.00;

    public String toString() {
        return String.format("%-12s%15.2f", "Egg", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
