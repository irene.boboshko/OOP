package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Anchovy extends Fillings {

    private double price = 24.15;

    public String toString() {
        return String.format("%-12s%15.2f", "Anchovy", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
