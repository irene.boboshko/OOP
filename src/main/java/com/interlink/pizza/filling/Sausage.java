package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Sausage extends Fillings {

    public String toString() {
        double price = 12.00;
        return String.format("%-12s%15.2f", "Sausage", price) + " UH";
    }
}
