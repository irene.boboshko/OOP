package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Mushrooms extends Fillings {

    private double price = 15.40;

    public String toString() {
        return String.format("%-12s%15.2f", "Mushrooms", this.price) + " UH";
    }

    public double calculate(int portion) {
        return portion* price;
    }
}
