package com.interlink.pizza.filling;

import com.interlink.pizza.Fillings;

public class Chicken extends Fillings {

    public String toString() {
        double price = 12.35;
        return String.format("%-12s%15.2f", "Chicken", price) + " UH";
    }
}
