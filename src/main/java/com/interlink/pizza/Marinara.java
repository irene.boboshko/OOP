package com.interlink.pizza;

class Marinara extends Pizza implements Orderable {

    public String getName() {
        return "Marinara";
    }

    private double getPrice() {
        return oregano.calculate(1) + anchovy.calculate(1) + garlic.calculate(1) + sauce.calculate(1) + dough.calculate(1);
    }

    @Override
    public String toString() {
        return String.format("-------Marinara-------\n%s\n%s\n%s\n%s\n%s\nTOTAL: %20.2f %s", dough, sauce, oregano, anchovy, garlic, getPrice(), "UH");
    }

    public double calculate(int portion) {
        return portion * getPrice();
    }
}
