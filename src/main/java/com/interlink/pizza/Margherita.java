package com.interlink.pizza;

class Margherita extends Pizza implements Orderable {

    public String getName() {
        return "Margherita";
    }

    private double getPrice() {
        return tomatoes.calculate(1) + mozzarella.calculate(1) + sweetBasil.calculate(1) + sauce.calculate(1) + dough.calculate(1);
    }

    @Override
    public String toString() {
        return String.format("-------Margherita-------\n%s\n%s\n%s\n%s\n%s\nTOTAL: %20.2f %s", dough, sauce, tomatoes, mozzarella, sweetBasil, getPrice(), "UH");
    }

    public double calculate(int portion) {
        return portion * getPrice();
    }
}
