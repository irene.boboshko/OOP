package com.interlink.pizza;

import java.util.Scanner;

class BrandedPizza implements Orderable {

    private Portion napoletanaPortion = new Portion();
    private Portion marinaraPortion = new Portion();
    private Portion marghPortion = new Portion();
    private Portion caprizziozaPortion = new Portion();

    void orderBrandedPizza() {
        Options options = new Options();
        Scanner scanner = new Scanner(System.in);
        options.selectPizza();
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("nap")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", napoletana.getName(), "Its price is -", napoletana.calculate(napoletanaPortion.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("mar")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", marinara.getName(), "Its price is -", marinara.calculate(marinaraPortion.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("mag")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", margherita.getName(), "Its price is -", margherita.calculate(marghPortion.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("cap")) {
                options.getPortionsOption();
                System.out.format("\n%s %s. %s %2.2f%s\n", "You've chosen", caprizzioza.getName(), "Its price is -", caprizzioza.calculate(caprizziozaPortion.getPortion()), "UH.");
                options.getCalculateOption();

            } else if (input.equalsIgnoreCase("calc")) {
                getBillForBrandedPizza();
                return;
            } else if (input.equalsIgnoreCase("quit")) {
                options.quit();
                return;
            } else {
                    options.showIncorrectInput();
                }
            }
        }

    private void getBillForBrandedPizza() {
        double sum = 0;
        System.out.printf("%-15s%-10s%-10s\n%s", "Name", "num", "price", "--------------------------------");
        if (napoletana.calculate(napoletanaPortion.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", napoletana.getName(), napoletanaPortion.getNum(),napoletana.calculate(napoletanaPortion.getNum()));
            sum += napoletana.calculate(napoletanaPortion.getNum());
        }
        if (marinara.calculate(marinaraPortion.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", marinara.getName(), marinaraPortion.getNum(), marinara.calculate(marinaraPortion.getNum()));
            sum += marinara.calculate(marinaraPortion.getNum());
        }
        if (margherita.calculate(marghPortion.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", margherita.getName(), marghPortion.getNum(), margherita.calculate(marghPortion.getNum()));
            sum += margherita.calculate(marghPortion.getNum());
        }
        if (caprizzioza.calculate(caprizziozaPortion.getNum()) != 0) {
            System.out.printf("\n%-15s%-10s%-10.2f", caprizzioza.getName(), caprizziozaPortion.getNum(), caprizzioza.calculate(caprizziozaPortion.getNum()));
            sum += caprizzioza.calculate(caprizziozaPortion.getNum());
        }
        System.out.printf("\n%s\n%s%26.2f", "--------------------------------", "TOTAL:", sum);
    }

}
