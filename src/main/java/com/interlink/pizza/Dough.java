package com.interlink.pizza;

class Dough extends Products {

    public String getName() {
        return "Dough";
    }

    private double price = 12.00;

    public double calculate(int portion) {
        return price*portion;
    }

    @Override
    public String toString() {
        return String.format("%-12s%15.2f", "Dough", this.price) + " UH";
    }
}
