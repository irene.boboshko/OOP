package com.interlink.pizza;

class Sauce extends Products {
    public String getName() {
        return "Sauce";
    }

    private double price = 8.00;

    public double calculate(int portion) {
        return price*portion;
    }

    @Override
    public String toString() {
        return String.format("%-12s%15.2f", "Sauce", this.price) + " UH";
    }
}
