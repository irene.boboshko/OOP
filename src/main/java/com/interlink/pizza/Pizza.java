package com.interlink.pizza;

import java.util.Scanner;

class Pizza implements Orderable {

    private BrandedPizza brandedPizza = new BrandedPizza();
    private SelfMadePizza ownPizza = new SelfMadePizza();
    private BrandedAddedPizza addedPizza = new BrandedAddedPizza();

    void welcome() {
        System.out.printf("Welcome to our pizzeria!\nTo get acquainted with our menu, press \"menu\".\nIf you'd like to exit, press \"quit\".\n");
        chooseMenu();
    }

    private void chooseMenu() {
        Options options = new Options();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("menu")) {
                showMenu();
                showProducts();
                System.out.printf("\nYou've got an opportunity\n\t *to order a branded pizza,\n\t *to make your own pizza,\n\t *to take a branded pizza with some additional ingredients.\nIf you are ready to make an order, press \"order\".\nTo exit the program, press \"quit\".\n\n");

            } else if (input.equalsIgnoreCase("order")) {
                orderPizza();
                return;
            } else if (input.equalsIgnoreCase("quit")) {
                System.out.println("Thank you for being with us!");
                return;
            } else {
                    options.showIncorrectInput();
                }
            }
        }

    private void showMenu() {
        System.out.format("%s\n\n%-12s%16s\n%s\n%s\n%s\n%s\n\n", "Branded pizza menu:", "PRODUCT", "PRICE", napoletana, marinara, margherita, caprizzioza);
    }

    private void showProducts() {
        System.out.format("%s\n\n%-12s%15s\n", "Available products to be added:", "PRODUCT", "PRICE");
        System.out.format("%s\n%s\n%s\n%s\n", "----------Spices-----------------", garlic, oregano, sweetBasil);
        System.out.format("%s\n%s\n%s\n%s\n%s\n%s\n", "----------Cheese-----------------", dutchCheese, gorgonzola, mozzarella, parmesan, ricotta);
        System.out.format("%s\n%s\n%s\n%s\n%s\n%s\n%s\n", "----------Main fillings----------", anchovy, chicken, egg, ham, mushrooms, sausage);
        System.out.format("%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n", "----------Vegetables-------------", artichoke, corn, olives, peas, pepper, spinach, tomatoes);
    }

    private void orderPizza() {
        System.out.println("To choose a branded pizza, press \"branded\".");
        System.out.println("To choose a branded pizza with some additional ingredients press \"add\".");
        System.out.println("To make your own pizza, press \"own\".\n");
        Scanner scanner = new Scanner(System.in);
        Options options = new Options();
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("branded")) {
                brandedPizza.orderBrandedPizza();
                return;
            } else if (input.equalsIgnoreCase("add")) {
                addedPizza.orderBrandedAddedPizza();
                return;
            } else if (input.equalsIgnoreCase("own")) {
                ownPizza.orderSelfMadePizza();
                return;
            } else if (input.equalsIgnoreCase("quit")) {
                options.quit();
                return;
            } else {
                options.showIncorrectInput();
            }
        }
    }
}

